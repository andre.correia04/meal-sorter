import click
import random
import sys
from datetime import datetime
from tabulate import tabulate

days = [ "Monday", 
    "Tuesday", 
    "Wednesday",
    "Thrusday",
    "Friday",
    "Saturday",
    "Sunday"]
    
meals = ["",
    "",
    "",
    "",
    "",
    "",
    ""]

to_buy = ["",
    "",
    "",
    "",
    "",
    "",
    ""]

l = ["",
    "",
    "",
    "",
    "",
    "",
    ""]

@click.group(invoke_without_command=True)
@click.argument("db")

def main(db):

    original_stdout = sys.stdout

    with open(db) as f:
        r = f.read()

    for d1 in range(days.__len__()):
        m=random.choice(r.split(','))
        f=m.split('-')
        
        while (f[0] in meals):
            m = random.choice(r.split(','))
            f=m.split('-')
        
        meals[d1] = f[0]
        to_buy[d1] = f[1]
    
    for d2 in range(days.__len__()):
        l[d2]= [days[d2],meals[d2],to_buy[d2]]

    table = tabulate(l, headers=['Day', 'Meal','To buy'], tablefmt='fancy_grid',stralign='center')

    filename = datetime.now().strftime("%d-%m-%Y") + "_shopping_list.txt"

    with open(filename, 'w') as f:
        sys.stdout = f # Change the standard output to the file we created.
        print(table)
        sys.stdout = original_stdout
    
    print(table)
    print("Shopping List at file:", filename)
    

if __name__ == '__main__':
    main()
